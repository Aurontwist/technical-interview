module.exports = function(grunt) {

	grunt.initConfig({

		pkg: grunt.file.readJSON('package.json'),

		// chech our JS
		jshint: {
			options: {
				"bitwise": true,
				"browser": true,
				"curly": true,
				"eqeqeq": true,
				"eqnull": true,
				"esnext": true,
				"immed": true,
				"jquery": true,
				"latedef": true,
				"newcap": true,
				"noarg": true,
				"node": true,
				"strict": false,
				"trailing": true,
				"undef": true,
				"globals": {
					"jQuery": true,
					"alert": true
				}
			},
			all: [
				'gruntfile.js',
				'../assets/js/app.js'
			]
		},

		// concat and minify our JS
		uglify: {
			dist: {
				files: {
					'../assets/js/script.min.js': [
						'../assets/js/app.js'
					]
				}
			}
		},

		// compile your sass
		sass: {
			dev: {
				options: {
					style: 'expanded',
                    require: 'susy'
				},
				src: ['../assets/scss/app.scss'],
				dest: '../assets/css/app.css'
			},
			prod: {
				options: {
					style: 'compressed',
                    require: 'susy'
				},
				src: ['../assets/scss/app.scss'],
				dest: '../assets/css/app.css'
			}
		},

        // Autoprefixer

        autoprefixer: {

            options: {
                browsers: ['last 2 versions', 'ie 8', 'ie 9']
            },

            no_dest: {
                src : ['../assets/css/app.css']
            }

        },

        // Spriting

        sprite:{
            all: {
                src: '../assets/img/sprites/src/*.png',
                destImg: '../assets/img/sprites/spritesheet.png',
                destCSS: '../assets/scss/components/_sprites.scss',
                imgPath: '../img/sprites/spritesheet.png'
            }
        },

		// watch for changes
		watch: {
			scss: {
				files: ['../assets/scss/**/*.scss'],
				tasks: [
					'sass:dev',
//					'sass:editorstyles',
					'notify:scss',
                    'autoprefixer'
				]
			},
			js: {
				files: [
					'<%= jshint.all %>'
				],
				tasks: [
					'jshint',
					'uglify',
					'notify:js'
				]
			},
            sprite: {
                files: ['../assets/img/sprites/src/*'],
                tasks:[ 'sprite', 'sass:dev']

            }

		},

		// notify cross-OS
		notify: {
			scss: {
				options: {
					title: 'Grunt, grunt!',
					message: 'SCSS is all gravy'
				}
			},
			js: {
				options: {
					title: 'Grunt, grunt!',
					message: 'JS is all good'
				}
			},
			dist: {
				options: {
					title: 'Grunt, grunt!',
					message: 'Theme ready for production'
				}
			}
		},

		clean: {
			dist: {
				src: ['../dist'],
				options: {
					force: true
				}
			}
		},

		copyto: {
			dist: {
				files: [
					{cwd: '../', src: ['**/*'], dest: '../dist/'}
				],
				options: {
					ignore: [
						'../dist{,/**/*}',
						'../doc{,/**/*}',
						'../grunt{,/**/*}',
						'../assets/scss{,/**/*}',
						'../*.map',
						'../bower_components{,/**/*}'

					]
				}
			}
		}
	});

	// Load NPM's via matchdep
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);


	// Development task
	grunt.registerTask('default', [
		'jshint',
		'uglify',
		'sass:dev',
//		'sass:editorstyles',
        'autoprefixer'
	]);

	// Production task
	grunt.registerTask('dist', function() {
		grunt.task.run([
			'jshint',
			'uglify',
			'sass:prod',
//			'sass:editorstyles',
            'autoprefixer',
			'clean:dist',
			'copyto:dist',
			'notify:dist'
		]);
	});
};